# WordPress Docker Environment
Includes WordPress, Mysql Database, Apache and WordPress Cli tool.


## Start the containers
This runs the containers in background
```
docker-compose up -d
```

## Install WordPress 
WordPress files are mappen inside the src/ folder. Installation happens automatically. Visit the page http://localhost:9696 to configure the admin user and complete the process.


## Use WP-CLI
Run the following command to see the wp-cli help page.
```
docker-compose run --rm wp-cli wp help

```

To run other WP-CLI commands:

```
docker-compose run --rm wp-cli wp <command>

```

